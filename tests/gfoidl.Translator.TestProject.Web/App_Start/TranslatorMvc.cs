using System.Linq;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using gfoidl.Translator.Mvc;
using gfoidl.Translator.TestProject.Web.App_Start;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(TranslatorMvc), "PreStart")]

namespace gfoidl.Translator.TestProject.Web.App_Start
{
    public static class TranslatorMvc
    {
        public static bool IsDebug
        {
            get
            {
#if DEBUG1
                return true;
#else
                return false;
#endif
            }
        }
        //---------------------------------------------------------------------
        public static void PreStart()
        {
#if !DEBUG1
            ViewEngines.Engines.Clear();
            ViewEngines.Engines.Add(new TranslatorRazorViewEnginge());
#endif
            RouteTable.Routes.IgnoreRoute("{*allLocalized}", new { allLocalized = @".*\.localized(/.*)?" });
        }
    }
}