﻿using System.Web.Mvc;

namespace gfoidl.Translator.TestProject.Web.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        public ActionResult Index()
        {
            this.ViewBag.Test = StringLocalizer.Get("Sprache");

            return this.View();
        }
        //---------------------------------------------------------------------
        public ActionResult Info()
        {
            return this.PartialView("_Info");
        }
    }
}