﻿/// <reference path="MyScripts1.ts" />
/// <reference path="typings/jquery/jquery.d.ts" />
var app;
(function (app) {
    (function (views) {
        var IndexPage = (function () {
            function IndexPage(triggerButtonSelector, divToChangeSelector) {
                this.ChangeTextOnClick(triggerButtonSelector, divToChangeSelector);
            }
            IndexPage.prototype.ChangeTextOnClick = function (triggerButtonSelector, divToChangeSelector) {
                $(triggerButtonSelector).click(function (e) {
                    var msg = "${eh guad}";
                    $(divToChangeSelector).html(msg);

                    var person = new views.Person("gfoidl", 33);
                    person.Show();
                });
            };
            return IndexPage;
        })();
        views.IndexPage = IndexPage;
    })(app.views || (app.views = {}));
    var views = app.views;
})(app || (app = {}));
