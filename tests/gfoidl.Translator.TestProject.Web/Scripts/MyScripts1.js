﻿var app;
(function (app) {
    (function (views) {
        var Person = (function () {
            function Person(name, age, male) {
                if (typeof male === "undefined") { male = true; }
                this.Name = name;
                this.Age = age;
                this.Male = male;
            }
            Person.prototype.Show = function () {
                alert("${Name}: " + this.Name + "\n${Alter}: " + this.Age + "\n${männlich}: " + this.Male);
            };
            return Person;
        })();
        views.Person = Person;
    })(app.views || (app.views = {}));
    var views = app.views;
})(app || (app = {}));
