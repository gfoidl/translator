﻿/// <reference path="../typings/jquery/jquery.d.ts" />
var views;
(function (views) {
    (function (home) {
        var Page = (function () {
            function Page() {
                this.Init();
            }
            Page.prototype.Init = function () {
                $("#my").html("${so ist das Leben}");
                $("#my").css("background", "yellow");
            };
            return Page;
        })();
        home.Page = Page;
    })(views.home || (views.home = {}));
    var home = views.home;
})(views || (views = {}));
