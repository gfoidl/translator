﻿/// <reference path="MyScripts1.ts" />
/// <reference path="typings/jquery/jquery.d.ts" />

module app.views {
    export class IndexPage {
        constructor(triggerButtonSelector: string, divToChangeSelector: string) {
            this.ChangeTextOnClick(triggerButtonSelector, divToChangeSelector);
        }

        private ChangeTextOnClick(triggerButtonSelector: string, divToChangeSelector: string): void {
            $(triggerButtonSelector).click(e => {
                var msg: string = "${eh guad}";
                $(divToChangeSelector).html(msg);

                var person = new Person("gfoidl", 33);
                person.Show();
            });
        }
    }
}