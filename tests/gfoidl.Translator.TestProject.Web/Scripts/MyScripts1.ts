﻿module app.views {
    export class Person {
        public Name: string;
        public Age: number;
        public Male: boolean;

        constructor(name: string, age: number, male: boolean = true) {
            this.Name = name;
            this.Age = age;
            this.Male = male;
        }

        public Show(): void {
            alert("${Name}: " + this.Name + "\n${Alter}: " + this.Age + "\n${männlich}: " + this.Male);
        }
    }
}