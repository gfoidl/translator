﻿using System.Collections.Generic;
using System.Web;
using NUnit.Framework;

namespace gfoidl.Translator.Mvc.Tests
{
    namespace LocalizedFileHandlerTests
    {
        [TestFixture]
        public class SetContentType
        {
            [Test]
            [TestCaseSource("File_requested___correct_ContentType_set_TestCases")]
            public string File_requested___correct_ContentType_set(string file)
            {
                HttpRequest request   = new HttpRequest(file, "http://test.at/" + file, null);
                HttpResponse response = new HttpResponse(null);
                HttpContext context   = new HttpContext(request, response);

                LocalizedFileHandler.SetContentType(context, file);

                return response.ContentType;
            }
            //---------------------------------------------------------------------
            private static IEnumerable<TestCaseData> File_requested___correct_ContentType_set_TestCases()
            {
                yield return new TestCaseData("foo.js").Returns("application/javascript");

                yield return new TestCaseData("bar.html").Returns("text/html");
                yield return new TestCaseData("bar.htm").Returns("text/html");

                yield return new TestCaseData("bild.png").Returns("image/png");
                yield return new TestCaseData("foto.jpg").Returns("image/jpeg");
            }
        }
    }
}