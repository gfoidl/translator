﻿using System;
using System.IO;
using NUnit.Framework;

namespace gfoidl.Translator.Mvc.Tests
{
    namespace PathHelperTests
    {
        [TestFixture]
        public class EnforceAppRelativePath
        {
            [Test]
            public void Path_given___Returns_correct_relative_path([Values("foo", "/foo", "~/foo")]string path)
            {
                string expected = "~/foo";
                string actual   = path.EnforceAppRelativePath();

                Assert.AreEqual(expected, actual);
            }
        }
        //---------------------------------------------------------------------
        [TestFixture]
        public class EnforceNoAppRelativePath
        {
            [Test]
            public void Path_given___Returns_correct_non_relative_path([Values("foo", "/foo", "~/foo")]string path)
            {
                string expected = "foo";
                string actual   = path.EnforceNoAppRelativePath();

                Assert.AreEqual(expected, actual);
            }
        }
        //---------------------------------------------------------------------
        [TestFixture]
        public class GetAvailableCulture
        {
            private static Func<string, string> _mapPath = s => Path.Combine(
                Environment.CurrentDirectory,
                s).Replace("/", "\\");

            private const string BaseDir = "trans\\Views";
            //---------------------------------------------------------------------
            [Test, SetCulture("de-AT")]
            public void Culture_where_Dir_doesnt_exist_but_parent_Dir_exists___Parent_culture_returned()
            {
                string expected = "de";
                string actual   = PathHelper.GetAvailableCulture(BaseDir, _mapPath);

                Assert.AreEqual(expected, actual);
            }
            //---------------------------------------------------------------------
            [Test, SetCulture("en-US")]
            public void Culture_where_Dir_exists___culture_returned()
            {
                string expected = "en-US";
                string actual   = PathHelper.GetAvailableCulture(BaseDir, _mapPath);

                Assert.AreEqual(expected, actual);
            }
            //---------------------------------------------------------------------
            [Test, SetCulture("ru")]
            public void Culture_where_no_Dir_exists___null_returned()
            {
                string actual = PathHelper.GetAvailableCulture(BaseDir, _mapPath);

                Assert.IsNull(actual);
            }
            //---------------------------------------------------------------------
            [Test, SetCulture("ru")]
            public void Culture_where_no_Dir_exists_and_stickToCulture_given___stickToCulture_returned()
            {
                string actual = PathHelper.GetAvailableCulture(BaseDir, _mapPath, "de-CH");

                Assert.AreEqual("de-CH", actual);
            }
        }
    }
}