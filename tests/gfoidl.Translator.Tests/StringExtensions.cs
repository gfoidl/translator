﻿namespace gfoidl.Translator.Tests
{
    internal static class StringExtensions
    {
        public static string NormalizeNewline(this string input)
        {
            return input.Replace("\r\n", "\n");
        }
    }
}