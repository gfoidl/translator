﻿using System;
using NUnit.Framework;

namespace gfoidl.Translator.Tests.ObjectLocalizerTest.Get
{
    [TestFixture]
    public class Arguments
    {
        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Type_is_null___Throws_ArgumentNull()
        {
            ObjectLocalizer.Get(null, new object(), "test");
        }
        //---------------------------------------------------------------------
        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Model_is_null___Throws_ArgumentNull()
        {
            ObjectLocalizer.Get(typeof(object), null, "test");
        }
        //---------------------------------------------------------------------
        [Test]
        [ExpectedException(typeof(ArgumentNullException))]
        public void PropertyName_is_null___Throws_ArgumentNull([Values(null, "", " ")] string arg)
        {
            ObjectLocalizer.Get(typeof(object), new object(), arg);
        }
    }
}