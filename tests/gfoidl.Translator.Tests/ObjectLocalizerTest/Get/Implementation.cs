﻿using System.Threading;
using System.Threading.Tasks;
using NUnit.Framework;

namespace gfoidl.Translator.Tests.ObjectLocalizerTest.Get
{
    [TestFixture]
    public class Implementation
    {
        public class Model
        {
            public string Name       { get; set; }
            public string Name_en    { get; set; }
            public string Prop1      { get; set; }
            public string Prop2      { get; set; }
            public string Prop2_en   { get; set; }
            public string Prop2_es   { get; set; }
            public string Prop3      { get; set; }
            public string Prop3_deCH { get; set; }
        }
        //---------------------------------------------------------------------
        private readonly Model _model;
        //---------------------------------------------------------------------
        public Implementation()
        {
            _model = new Model
            {
                Name       = "Apfel",
                Name_en    = "Apple",
                Prop1      = "Eigenschaft",
                Prop2      = "Auto",
                Prop2_en   = "Car",
                Prop2_es   = "Coche",
                Prop3      = "Haus",
                Prop3_deCH = "Häusli"
            };
        }
        //---------------------------------------------------------------------
        [Test, Repeat(2)]
        [SetCulture("de-AT")]
        public void Generic_Default_Culture___Returns_de_Property()
        {
            string actual = ObjectLocalizer.Get(_model, "Name");

            Assert.AreEqual("Apfel", actual);
        }
        //---------------------------------------------------------------------
        [Test]
        [SetCulture("de-AT")]
        public void NonGeneric_Default_Culture___Returns_de_Property()
        {
            string actual = ObjectLocalizer.Get(typeof(Model), _model, "Name");

            Assert.AreEqual("Apfel", actual);
        }
        //---------------------------------------------------------------------
        [Test]
        [SetCulture("de-CH")]
        public void Localized_Property_found___Returns_localized_Property()
        {
            string actual = ObjectLocalizer.Get(_model, "Prop3");

            Assert.AreEqual("Häusli", actual);
        }
        //---------------------------------------------------------------------
        [Test]
        [SetCulture("it")]
        public void Localized_Property_not_existing___Returns_de_Property()
        {
            string actual = ObjectLocalizer.Get(_model, "Prop1");

            Assert.AreEqual("Eigenschaft", actual);
        }
        //---------------------------------------------------------------------
        [Test]
        [SetCulture("it")]
        public void Localized_Property_not_found___Returns_de_Property()
        {
            string actual = ObjectLocalizer.Get(_model, "Name");

            Assert.AreEqual("Apfel", actual);
        }
        //---------------------------------------------------------------------
        [Test]
        [SetCulture("en-US")]
        public void Localized_Property_not_found_but_parent_Property___Returns_parent_Property()
        {
            string actual = ObjectLocalizer.Get(_model, "Prop2");

            Assert.AreEqual("Car", actual);
        }
        //---------------------------------------------------------------------
        [Test, Explicit("May result in a deadlock on AppVeyor")]
        [SetCulture("de-AT")]
        public void Thread_Attack___OK()
        {
            string actual = ObjectLocalizer.Get(_model, "Name");
            Assert.AreEqual("Apfel", actual);

            const int count = 10;
            using (ManualResetEventSlim mre = new ManualResetEventSlim())
            using (CountdownEvent cde0      = new CountdownEvent(count))
            using (CountdownEvent cde1      = new CountdownEvent(count))
            {
                Task.Run(() =>      // sonst blockiert der Main-Thread
                {
                    Parallel.For(0, count, i =>
                    {
                        cde0.Signal();
                        mre .Wait();

                        string actual1 = ObjectLocalizer.Get(_model, "Name");
                        Assert.AreEqual("Apfel", actual1);

                        cde1.Signal();
                    });
                });

                cde0.Wait();
                mre .Set();
                cde1.Wait();
            }
        }
    }
}