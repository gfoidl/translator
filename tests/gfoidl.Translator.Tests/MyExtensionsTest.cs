﻿using System.IO;
using System.Linq;
using System.Text;
using NUnit.Framework;

namespace gfoidl.Translator.Tests
{
    namespace MyExtensionsTest
    {
        [TestFixture]
        public class EnsureQuotesByMultiline
        {
            [Test]
            [TestCase(null  , Result = null)]
            [TestCase(""    , Result = "")]
            [TestCase("a\tb", Result = "a\tb")]
            [TestCase("abc" , Result = "abc")]
            [TestCase("a\nb", Result = "\"a\nb\"")]
            public string Input_given___returns_correct_quoted_string(string input)
            {
                return input.EnsureQuotesByMultiline();
            }
        }
        //---------------------------------------------------------------------
        [TestFixture]
        public class ReadColsFromMultiline
        {
            [Test]
            public void Seperator_within_quotes___is_not_treated_as_seperator_Issue8()
            {
                string template = @"""bei denen mindestens ein Prozentwert &ge; 80% ist""";
                StringReader sr = new StringReader(template);

                var actual = sr.ReadColsFromMultiline().ToList();

                Assert.AreEqual(1, actual.Count);
                Assert.AreEqual(1, actual[0].Count);
                Assert.AreEqual("bei denen mindestens ein Prozentwert &ge; 80% ist", actual[0][0]);
            }
            //---------------------------------------------------------------------
            [Test]
            public void Quotes_within_line___treated_as_text_Issue12()
            {
                string template = @"ein Text mit ""Markierung"" soll auch gehen";
                StringReader sr = new StringReader(template);

                var actual = sr.ReadColsFromMultiline().ToList();

                Assert.AreEqual(1, actual.Count);
                Assert.AreEqual(1, actual[0].Count);
                Assert.AreEqual(@"ein Text mit ""Markierung"" soll auch gehen", actual[0][0]);
            }
            //---------------------------------------------------------------------
            [Test]
            public void Quotes_within_line_in_quotes___treated_as_text_Issue12()
            {
                string template = @"""ein Text mit ""Markierung"" soll auch gehen""";
                StringReader sr = new StringReader(template);

                var actual = sr.ReadColsFromMultiline().ToList();

                Assert.AreEqual(1, actual.Count);
                Assert.AreEqual(1, actual[0].Count);
                Assert.AreEqual(@"ein Text mit ""Markierung"" soll auch gehen", actual[0][0]);
            }
            //---------------------------------------------------------------------
            [Test]
            public void Quotes_not_as_multiline_marker___treated_as_text_Issue12()
            {
                string template = @"ein ""Text"" der;Anführungszeichen enthält";
                StringReader sr = new StringReader(template);

                var actual = sr.ReadColsFromMultiline().ToList();

                Assert.AreEqual(1, actual.Count);
                Assert.AreEqual(2, actual[0].Count);
                Assert.AreEqual(@"ein ""Text"" der", actual[0][0]);
                Assert.AreEqual("Anführungszeichen enthält", actual[0][1]);
            }
            //---------------------------------------------------------------------
            [Test]
            [TestCase("# Das ist ein Kommentar")]
            [TestCase("#Das ist ein Kommentar")]
            public void CommentMarker_at_begin_of_line___line_is_ignored(string template)
            {
                StringReader sr = new StringReader(template);

                var actual = sr.ReadColsFromMultiline().ToList();

                Assert.AreEqual(0, actual.Count);
            }
            //---------------------------------------------------------------------
            [Test]
            public void CommentMarker_in_Token___line_read()
            {
                string template = "abc;kein # Kommentar";
                StringReader sr = new StringReader(template);

                var actual = sr.ReadColsFromMultiline().ToList();

                Assert.AreEqual(1, actual.Count);
                Assert.AreEqual("abc", actual[0][0]);
                Assert.AreEqual("kein # Kommentar", actual[0][1]);
            }
            //---------------------------------------------------------------------
            [Test]
            public void Comment_and_normal___OK()
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine("#Kommentar");
                sb.AppendLine("abc;das nicht");
                string template = sb.ToString();
                StringReader sr = new StringReader(template);

                var actual = sr.ReadColsFromMultiline().ToList();

                Assert.AreEqual(1, actual.Count);
                Assert.AreEqual("abc", actual[0][0]);
                Assert.AreEqual("das nicht", actual[0][1]);
            }
            //---------------------------------------------------------------------
            [Test]
            public void Empty_line___ignored()
            {
                StringBuilder sb = new StringBuilder();
                sb.AppendLine();
                sb.AppendLine("abc;das nicht");
                sb.AppendLine();
                string template = sb.ToString();
                StringReader sr = new StringReader(template);

                var actual = sr.ReadColsFromMultiline().ToList();

                Assert.AreEqual(1, actual.Count);
                Assert.AreEqual("abc", actual[0][0]);
                Assert.AreEqual("das nicht", actual[0][1]);
            }
        }
    }
}