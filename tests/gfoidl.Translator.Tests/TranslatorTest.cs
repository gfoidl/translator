﻿using System;
using System.Collections.Generic;
using System.IO;
using NUnit.Framework;

namespace gfoidl.Translator.Tests
{
    namespace TranslatorTest
    {
        [TestFixture]
        public class Ctor
        {
            [Test]
            [ExpectedException(typeof(ArgumentNullException))]
            public void Dictionary_is_null___Throws_ANE()
            {
                Translator sut = new Translator(null);
            }
        }
        //---------------------------------------------------------------------
        [TestFixture]
        public class Translate
        {
            [Test]
            [ExpectedException(typeof(ArgumentNullException))]
            public void File_is_null___Throws_ANE()
            {
                Translator sut = new Translator(new Dictionary<string, string>());

                sut.Translate(null as string);
            }
            //---------------------------------------------------------------------
            [Test]
            [ExpectedException(typeof(FileNotFoundException))]
            public void File_does_not_exist___Throws_FileNotFound()
            {
                Translator sut = new Translator(new Dictionary<string, string>());

                sut.Translate("abc.txt");
            }
        }
        //---------------------------------------------------------------------
        [TestFixture]
        public class TranslateInternal
        {
            [Test]
            public void Dic_and_Content_given___OK()
            {
                Dictionary<string, string> dic = new Dictionary<string, string>
                {
                    {"Hallo", "Hello"},
                    {"Welt", "world"}
                };
                Translator sut = new Translator(dic);

                string content  = @"${Hallo} ${Welt}";
                string expected = "Hello world";

                string actual = sut.TranslateInternal(content);

                Assert.AreEqual(expected, actual);
            }
            //---------------------------------------------------------------------
            [Test]
            public void No_all_tokens_in_Dic___these_tokens_not_translated()
            {
                Dictionary<string, string> dic = new Dictionary<string, string>
                {
                    {"Hallo", "Hello"},
                    {"Welt", "world"}
                };
                Translator sut = new Translator(dic);

                string content  = @"${Hallo} ${du} ${Welt}";
                string expected = "Hello ${du} world";

                string actual = sut.TranslateInternal(content);

                Assert.AreEqual(expected, actual);
            }
        }
    }
}