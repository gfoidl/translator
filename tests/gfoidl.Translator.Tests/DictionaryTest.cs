﻿using System;
using System.Collections.Generic;
using System.IO;
using NUnit.Framework;

namespace gfoidl.Translator.Tests
{
    namespace DictionaryTest
    {
        [TestFixture]
        public class Ctor
        {
            [Test]
            public void FileName_is_null___Throws_ANE()
            {
                string paramName = null;
                try
                {
                    Dictionary sut = new Dictionary(null);
                }
                catch (ArgumentNullException ex)
                {
                    paramName = ex.ParamName;
                }

                Assert.AreEqual("fileName", paramName);
            }
            //---------------------------------------------------------------------
            [Test]
            public void FileName_given___OK()
            {
                Dictionary sut = new Dictionary("foo.txt");

                Assert.AreEqual("foo.txt", sut.FileName);
            }
            //---------------------------------------------------------------------
            [Test]
            public void FileName_points_to_folder___DictionaryCsv_is_used()
            {
                Dictionary sut = new Dictionary("foo");

                Assert.AreEqual(@"foo\dictionary.csv", sut.FileName);
            }
            //---------------------------------------------------------------------
            [Test]
            public void HasHeader_is_given___Correct_value([Values(true, false)] bool hasHeader)
            {
                Dictionary sut = new Dictionary("bar.txt", hasHeader);

                Assert.AreEqual(hasHeader, sut.HasHeader);
            }
        }
        //---------------------------------------------------------------------
        [TestFixture]
        public class Culture
        {
            [Test]
            public void FileName_given___Culture_is_filename_without_path_and_extension(
                [Values("foo.txt", "data/foo.txt", "data\\foo.txt")] string fileName)
            {
                Dictionary sut = new Dictionary(fileName);

                Assert.AreEqual("foo", sut.Culture);
            }
        }
        //---------------------------------------------------------------------
        [TestFixture]
        public class WriteTokensToCsv
        {
            [Test]
            [ExpectedException(typeof(ArgumentNullException))]
            public void Tokens_is_null___Throws_ANE()
            {
                Dictionary sut = new Dictionary("abc.txt");

                sut.WriteTokensToCsv(null);
            }
            //---------------------------------------------------------------------
            [Test]
            public void Tokens_given_but_Folder_does_not_exist___Folder_created()
            {
                if (Directory.Exists("data1"))
                    Directory.Delete("data1", true);

                string[] tokens = { "abc", "xyz" };
                Dictionary sut = new Dictionary("data1/dic.csv");

                sut.WriteTokensToCsv(tokens);

                string actual = File.ReadAllText("data1/dic.csv");

                string expected =
@"token;translation
abc;abc
xyz;xyz
";

                Assert.AreEqual(expected.NormalizeNewline(), actual.NormalizeNewline());
            }
            //---------------------------------------------------------------------
            [Test]
            public void Tokens_given_default___File_written_with_header_and_dummy_appended()
            {
                string[] tokens = { "abc", "xyz" };
                Dictionary sut = new Dictionary("data/dic.csv");

                sut.WriteTokensToCsv(tokens);

                string actual = File.ReadAllText("data/dic.csv");

                string expected =
@"token;translation
abc;abc
xyz;xyz
";

                Assert.AreEqual(expected.NormalizeNewline(), actual.NormalizeNewline());
            }
            //---------------------------------------------------------------------
            [Test]
            public void Tokens_given_no_header___File_written_with_dummy_appended()
            {
                string[] tokens = { "abc", "xyz" };
                Dictionary sut = new Dictionary("data/dic.csv", false);

                sut.WriteTokensToCsv(tokens);

                string actual = File.ReadAllText("data/dic.csv");

                string expected =
@"abc;abc
xyz;xyz
";

                Assert.AreEqual(expected.NormalizeNewline(), actual.NormalizeNewline());
            }
            //---------------------------------------------------------------------
            [Test]
            public void Tokens_given_no_append___File_written_with_header()
            {
                string[] tokens = { "abc", "xyz" };
                Dictionary sut = new Dictionary("data/dic.csv");

                sut.WriteTokensToCsv(tokens, false);

                string actual = File.ReadAllText("data/dic.csv");

                string expected =
@"token;translation
abc;
xyz;
";

                Assert.AreEqual(expected.NormalizeNewline(), actual.NormalizeNewline());
            }
            //---------------------------------------------------------------------
            [Test]
            public void Tokens_given_default_Tab_as_separator___File_written_with_header_and_dummy_appended_and_Tab(
                [Values('\t', '|')] char separator)
            {
                string[] tokens = { "abc", "xyz" };
                Dictionary sut = new Dictionary("data/dic.csv");

                sut.WriteTokensToCsv(tokens, separator: separator);

                string actual = File.ReadAllText("data/dic.csv");

                string expected =
@"token;translation
abc;abc
xyz;xyz
";
                expected = expected.Replace(';', separator);

                Assert.AreEqual(expected.NormalizeNewline(), actual.NormalizeNewline());
            }
            //---------------------------------------------------------------------
            [Test]
            public void Tokens_with_multiline_given___multiline_text_enclosed_in_apostrophs()
            {
                string[] tokens = { "abc", "Das hier ist ein\nsehr langer\nText" };
                Dictionary sut = new Dictionary("data/dic.csv");

                sut.WriteTokensToCsv(tokens);

                string actual = File.ReadAllText("data/dic.csv");

                string expected =
@"token;translation
abc;abc
""Das hier ist ein
sehr langer
Text"";""Das hier ist ein
sehr langer
Text""
";
                
                Assert.AreEqual(expected.NormalizeNewline(), actual.NormalizeNewline());
            }
        }
        //---------------------------------------------------------------------
        [TestFixture]
        public class ReadTokensFromCsv
        {
            [Test]
            [ExpectedException(typeof(ArgumentOutOfRangeException))]
            public void Column_lower_than_1___Throws_ArgumentOutOfRange()
            {
                Dictionary sut = new Dictionary("abc");

                var actual = sut.ReadTokensFromCsv(0);
            }
            //---------------------------------------------------------------------
            [Test]
            [ExpectedException(typeof(ArgumentException))]
            public void Read_column_not_found___Throws_Argument(
                [Values("data/de_header.csv", "data/de_no_header.csv")] string fileName,
                [Values(true, false)] bool hasHeader)
            {
                Dictionary sut = new Dictionary(fileName, hasHeader);

                var actual = sut.ReadTokensFromCsv(2);
            }
            //---------------------------------------------------------------------
            [Test]
            public void Read_no_header_default_column___OK()
            {
                Dictionary sut = new Dictionary("data/de_no_header.csv", false);

                var actual = sut.ReadTokensFromCsv();

                Assert.IsTrue(IsCorrect(actual));
            }
            //---------------------------------------------------------------------
            [Test]
            public void Read_with_header_default_column___OK()
            {
                Dictionary sut = new Dictionary("data/de_header.csv");

                var actual = sut.ReadTokensFromCsv();

                Assert.IsTrue(IsCorrect(actual));
            }
            //---------------------------------------------------------------------
            [Test]
            public void Read_no_header_column_2___OK()
            {
                Dictionary sut = new Dictionary("data/fake_de.csv", false);

                var actual = sut.ReadTokensFromCsv(2);

                Assert.IsTrue(IsCorrect(actual));
            }
            //---------------------------------------------------------------------
            [Test]
            public void Read_no_header_default_column_Tab_as_separator___OK()
            {
                Dictionary sut = new Dictionary("data/de_no_header_tab.csv", false);

                var actual = sut.ReadTokensFromCsv(separator: '\t');

                Assert.IsTrue(IsCorrect(actual));
            }
            //---------------------------------------------------------------------
            private static bool IsCorrect(Dictionary<string, string> actual)
            {
                var expected = new Dictionary<string, string>
                {
                    {"Header", "Überschrift"},
                    {"Name", "Name"},
                    {"Age", "Alter"},
                    {"Sex", "Geschlecht"},
                    {"male", "männlich"},
                    {"female", "weiblich"},
                    {"es ist schön", "Das Leben ist so <strong>schön</strong>."}
                };

                foreach (var key in expected.Keys)
                {
                    string value;
                    if (!actual.TryGetValue(key, out value))
                        return false;

                    if (value != expected[key]) return false;
                }

                return true;
            }
            //---------------------------------------------------------------------
            [Test]
            public void Read_multiline_default___OK_and_Apostrophs_are_stripped()
            {
                Dictionary sut = new Dictionary("data/multiline.csv");

                var actual = sut.ReadTokensFromCsv();

                Assert.AreEqual("xyz", actual["abc"]);
                Assert.AreEqual("lmn", actual["ijk"]);
                Assert.AreEqual("Another\nlong Text", actual["lang"]);
                Assert.AreEqual("This here is a\nvery long\nText", actual["Das hier ist ein\nsehr langer\nText"]);
            }
            //---------------------------------------------------------------------
            [Test]
            public void Read_multiline_no_headers___OK_and_Apostrophs_are_stripped()
            {
                Dictionary sut = new Dictionary("data/multiline.csv", false);

                var actual = sut.ReadTokensFromCsv();

                Assert.AreEqual("xyz", actual["abc"]);
                Assert.AreEqual("lmn", actual["ijk"]);
                Assert.AreEqual("Another\nlong Text", actual["lang"]);
                Assert.AreEqual("This here is a\nvery long\nText", actual["Das hier ist ein\nsehr langer\nText"]);
            }
        }
    }
}