﻿using System;
using System.IO;
using System.Linq;
using NUnit.Framework;

namespace gfoidl.Translator.Tests
{
    namespace DictionaryMergerTest
    {
        [TestFixture]
        public class Ctor
        {
            [Test]
            public void Filename_is_null___Throws_ANE()
            {
                string paramName = null;
                try
                {
                    DictionaryMerger sut = new DictionaryMerger(null);
                }
                catch (ArgumentNullException ex)
                {
                    paramName = ex.ParamName;
                }

                Assert.AreEqual("mergedDictionaryFile", paramName);
            }
            //---------------------------------------------------------------------
            [Test]
            public void Filename_given___OK()
            {
                DictionaryMerger sut = new DictionaryMerger("foo.txt");

                Assert.AreEqual("foo.txt", sut.MergedDicionaryFile);
            }
        }
        //---------------------------------------------------------------------
        [TestFixture]
        public class GetOrderedDics
        {
            [Test]
            public void Dictionaries_given___Correct_Cultures_returned()
            {
                Dictionary[] dictionaries =
                {
                    new Dictionary("data/de.csv"),
                    new Dictionary("data/en.csv")
                };

                string[] expected = { "de", "en" };
                var actual        = DictionaryMerger.GetOrderedDics(dictionaries).Select(t => t.Item1);

                CollectionAssert.AreEqual(expected, actual);
            }
            //---------------------------------------------------------------------
            [Test]
            public void Dictionaries_in_different_order_given___Cultures_ordered()
            {
                Dictionary[] dictionaries =
                {
                    new Dictionary("data/en.csv"),
                    new Dictionary("data/de.csv"),
                    new Dictionary("data/al.csv"),
                    new Dictionary("data/ru.csv")
                };

                string[] expected = { "al", "de", "en", "ru" };
                var actual        = DictionaryMerger.GetOrderedDics(dictionaries).Select(t => t.Item1);

                CollectionAssert.AreEqual(expected, actual);
            }
        }
        //---------------------------------------------------------------------
        [TestFixture]
        public class RetrieveTokens
        {
            [Test]
            public void Dictionaries_given___Correct_Tokens_returned()
            {
                Dictionary[] dictionaries =
                {
                    new Dictionary("data/de.csv"),
                    new Dictionary("data/en.csv")
                };

                var dics = dictionaries.Select(d => d.ReadTokensFromCsv());

                string[] expected = { "_divColor", "Hallo Welt, wie geht es dir?", "Tirol", "foo" };
                string[] actual   = DictionaryMerger.RetrieveTokens(dics);

                CollectionAssert.AreEquivalent(expected, actual);
            }
            //---------------------------------------------------------------------
            [Test]
            public void Dictionaries_given___Tokens_ordered()
            {
                Dictionary[] dictionaries =
                {
                    new Dictionary("data/de.csv"),
                    new Dictionary("data/en.csv")
                };

                var dics = dictionaries.Select(d => d.ReadTokensFromCsv());

                string[] expected = { "_divColor", "foo", "Hallo Welt, wie geht es dir?", "Tirol" };
                string[] actual   = DictionaryMerger.RetrieveTokens(dics);

                CollectionAssert.AreEqual(expected, actual);
            }
        }
        //---------------------------------------------------------------------
        [TestFixture]
        public class MergeDictionaries
        {
            [Test]
            public void Dictionaries_given___Correct_Merged_Dic_generated()
            {
                Dictionary[] dics =
                {
                    new Dictionary("data/de.csv"),
                    new Dictionary("data/en.csv")
                };

                DictionaryMerger sut = new DictionaryMerger("data/merged_actual.csv");
                sut.MergeDictionaries(dics);

                string expected = File.ReadAllText("data/merged_expected.csv");
                string actual   = File.ReadAllText("data/merged_actual.csv");

                Assert.AreEqual(expected.NormalizeNewline(), actual.NormalizeNewline());
            }
            //---------------------------------------------------------------------
            [Test]
            public void Dictionaries_in_different_order_given___Merge_is_sorted_by_culture()
            {
                Dictionary[] dics =
                {
                    new Dictionary("data/en.csv"),
                    new Dictionary("data/de.csv")
                };

                DictionaryMerger sut = new DictionaryMerger("data/merged_actual.csv");
                sut.MergeDictionaries(dics);

                string expected = File.ReadAllText("data/merged_expected.csv");
                string actual   = File.ReadAllText("data/merged_actual.csv");

                Assert.AreEqual(expected.NormalizeNewline(), actual.NormalizeNewline());
            }
        }
        //---------------------------------------------------------------------
        [TestFixture]
        public class SplitDictionary
        {
            [Test]
            public void OutputPath_is_null___ThrowsANE()
            {
                DictionaryMerger sut = new DictionaryMerger("foo");
                string paramName     = null;

                try
                {
                    sut.SplitDictionary(null);
                }
                catch (ArgumentNullException ex)
                {
                    paramName = ex.ParamName;
                }

                Assert.AreEqual("outputPath", paramName);
            }
            //---------------------------------------------------------------------
            [Test]
            public void Merged_dic_given___Ditionaries_written_fo_disk()
            {
                DictionaryMerger sut = new DictionaryMerger("data/merged_expected.csv");
                sut.SplitDictionary("data/splitted");

                string[] cultues = { "de", "en" };

                foreach (string cul in cultues)
                {
                    string expected = File.ReadAllText("data/splitted/" + cul + "_expected.csv");
                    string actual   = File.ReadAllText("data/splitted/" + cul + ".csv");

                    Assert.AreEqual(expected.NormalizeNewline(), actual.NormalizeNewline());
                }
            }
        }
    }
}