﻿using System;
using NUnit.Framework;

namespace gfoidl.Translator.Tests
{
    namespace StringLocalizerTest
    {
        [TestFixture, Explicit("Edit app.config and remove 'dictionaries'")]
        public class Init
        {
            [Test]
            [ExpectedException(typeof(ArgumentNullException))]
            public void No_dictionaries_set_in_config___Throws_ArgumentNull()
            {
                Type type = typeof(StringLocalizer);
                
                Assert.IsNotNull(type);
            }
        }
        //---------------------------------------------------------------------
        [TestFixture]
        public class Get
        {
            [Test]
            [ExpectedException(typeof(ArgumentNullException))]
            public void Token_is_null___Throws_ArgumentNull()
            {
                string actual = StringLocalizer.Get(null);
            }
            //---------------------------------------------------------------------
            [Test, SetCulture("de-AT")]
            public void Culture_not_in_dic_but_parent___Correct_String_returned()
            {
                string expected = "Hallo Welt,\nwie geht es dir?";

                string actual = StringLocalizer.Get("Hallo Welt, wie geht es dir?");

                Assert.AreEqual(expected, actual);
            }
            //---------------------------------------------------------------------
            [Test, SetCulture("en")]
            public void Culture_in_dic___Correct_String_returned()
            {
                string expected = "Hello world, how are you?";

                string actual = StringLocalizer.Get("Hallo Welt, wie geht es dir?");

                Assert.AreEqual(expected, actual);
            }
            //---------------------------------------------------------------------
            [Test, SetCulture("fr")]
            public void Culture_not_in_dic___Default_Culture_used()
            {
                string expected = "Hallo Welt,\nwie geht es dir?";

                string actual = StringLocalizer.Get("Hallo Welt, wie geht es dir?");

                Assert.AreEqual(expected, actual);
            }
            //---------------------------------------------------------------------
            [Test]
            [ExpectedException(typeof(ArgumentException))]
            public void Token_not_in_dic___Throws_ArgumentException()
            {
                string actual = StringLocalizer.Get(Guid.NewGuid().ToString());
            }
        }
    }
}