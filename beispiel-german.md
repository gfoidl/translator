This text is only available in German -- it's taken from an email to a collegue. I'll have to translate it first.

# Allgemeines

Das Tool ist ein Kommandozeilenprogramm, dass aus Text-Dateien markierte Token extrahieren kann und anhand dieser Token und deren Übersetzung eine Text-Datei lokalisieren (=übersetzen) kann.

Die Text-Dateien müssen UTF-8 kodiert sein.

Nachfolgende Schritte erkläre ich am Beispiel einer einfachen Text-Datei (my-text.txt), die in Deutsch und Englisch verfügbar sein soll.

Gezeigt werden die Schritte hier anhand einer Vorlagen-Datei. Es kann aber statt der einzelnen Datei auch ein Ordner übergeben werden, dann werden alle Dateien im Ordner verarbeitet.

Hier wird auch immer im aktuellen Ordner gearbeitet. Es kann natürlich jeder Ordner sein.
```
Allgemeines
===========

Dieser Text soll übersetzt werden.

Es soll auch mehrzeiliger
Text möglich sein.
```
Der Text kann natürlich belieber Text sein, also auch xml, html, xdp, usw.

# Vorbereitung
Der Text bzw. die Vorlage/Template muss vorbereitet werden. Dazu wird der Text in sogenannte _Tokens_ zerlegt. Ein Token wird mit `${` und `}` markiert. 
```
${Allgemeines}
===========

${Dieser Text soll übersetzt werden}.

${Es soll auch mehrzeiliger
Text möglich sein.}
```
Wieviel Text ein Token umfasst ist egal -- wie es praktisch ist. Es kann ein Wort sein, eine Zeile, ein Absatz, egal. Es könnte auch der gesamte Text sein, nur das macht i.d.R. keinen Sinn.

# Extrahierung der Tokens
Anschließend werden die Tokens extrahiert und in eine Datei geschrieben, die dann übersetzt werden kann.  
`mono Translator.exe extract ./my-text.txt ./dic.csv -v`  
Die Tokens werden extrahiert und in das _Wörterbuch_ dic.csv geschrieben.
`-v` steht dabei für _verbose_ -- es werden Infos ausgegeben.

Der Inhalt von dic.csv ist:
```
token;translation
Allgemeines;Allgemeines
Dieser Text soll übersetzt werden;Dieser Text soll übersetzt werden
"Es soll auch mehrzeiliger
Text möglich sein.";"Es soll auch mehrzeiliger
Text möglich sein."
```

# Übersetzung der Tokens
Die Datei dic.csv kann z.B. mittels Excel / LibreOffice / etc. übersetzt werden.  
![ijdcgifh.png](https://bitbucket.org/repo/5ad8q8/images/946374368-ijdcgifh.png)
```
token;de;en
Allgemeines;Allgemeines;General
Dieser Text soll übersetzt werden;Dieser Text soll übersetzt werden;This text shall be translated
"Es soll auch mehrzeiliger
Text möglich sein.";"Es soll auch mehrzeiliger
Text möglich sein.";"Multiline text 
is also possible."
```
Schaut zwar so wegen den mehrzeiligen Inhalten nicht so super aus, aber das wäre die (manuelle) Übersetzung der Token in die Sprachen _de_ und _en_.

Es kann vorteilhaft sein, diese Datei unter einem andern Namen zu Speichern, nicht dass sie ggf. überschrieben wird wenn das Tool erneut ausgeführt wird.

## Splitten der Übersetzung
Mit der so übersetzten Datei dic.csv kann der nächste Schritt durchgeführt werden.  
Es kann aber auch sein, dass pro Kultur (also de od. en) eine eigene Datei gewünscht ist -- wie erwähnt: muss nicht sein, kann aber u.U. praktischer sein. Dieses Splitten geht mit  
`mono Translator split ./dic.csv ./splitted -v`  
So wird im Verzeichnis ./splitted je eine Datei de.csv und en.csv mit nur der jeweiligen Sprache erstellt.

# Übersetzung der Vorlagen
Nachdem die Vorlagen vorbereitet sind, die Tokens extrahiert und die Tokens übersetzt sind, kann die Vorlage (bzw. die Vorlagen wenn ein Ordner übergeben wird) übersetzt werden. Die Vorlage wird dabei überschrieben! Daher u.U. die Vorlage vorher z.B. in eine Ordner _de_ und _en_ kopieren. `cp ./my-text.txt ./my-text.de.txt` und `cp ./my-text.txt ./my-test.en.txt` od. wie es halt gewünscht ist.

Das Übersetzung nach deutsch geht mit:  
`mono Translator translate ./my-text.de.txt ./dic.csv 1 -v`  
1 ist dabei die Spaltennummer (0-basiert) im Wörterbuch dic.csv.

Für Englisch wäre es somit:  
`mono Translator translate ./my-text.en.txt ./dic.csv 2 -v`  

Für englisch my-text.en.txt kommt dann das raus:  
```
General
===========

This text shall be translated.

Multiline text 
is also possible.
```

# Vorteil
Die Vorlage ist für alle Kulturen gleich und kann einfach weiterentwickelt werden.  
Im Wörterbuch dic.csv brauchen nur die neuen Token, die anfallen, aufgenommen werden. Diese können natürlich auch -- wie oben gezeigt -- mit _extract_ extrahiert werden und so in das Wörterbuch eingepflegt werden. 

Die Schritte extrahieren, übersetzen lassen sich gut in Build-Scripte bzw. Build-Tools integrieren.
