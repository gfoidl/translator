﻿using System.Reflection;

[assembly: AssemblyCompany("Foidl Günther")]
[assembly: AssemblyProduct("gfoidl.Translator")]
[assembly: AssemblyCopyright("Copyright © Foidl Günther 2016-2018")]

[assembly: AssemblyVersion("1.5.74.1")]
[assembly: AssemblyInformationalVersion("1.5.1")]
