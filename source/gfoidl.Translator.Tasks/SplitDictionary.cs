﻿using System;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;

namespace gfoidl.Translator.Tasks
{
    public class SplitDictionary : Task
    {
        [Required]
        public string MergedDictionary { get; set; }

        [Required]
        public string OutputPath { get; set; }
        //---------------------------------------------------------------------
        public override bool Execute()
        {
#if DEBUG
            this.Log.LogMessage("# MergedDictionary = {0}\n# OutputPath = {1}", this.MergedDictionary, this.OutputPath);
#endif
            try
            {
                DictionaryMerger splitter = new DictionaryMerger(this.MergedDictionary);
                splitter.SplitDictionary(this.OutputPath);
            }
            catch (Exception ex)
            {
                this.Log.LogErrorFromException(ex);
            }

            return !this.Log.HasLoggedErrors;
        }
    }
}