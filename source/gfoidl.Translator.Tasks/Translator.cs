﻿using System;
using System.IO;
using System.Linq;
using Microsoft.Build.Framework;
using Microsoft.Build.Utilities;

namespace gfoidl.Translator.Tasks
{
    public class Translator : Task
    {
        [Required]
        public string Dictionary { get; set; }

        public bool DictionaryHasHeader { get; set; }
        public int ColumnInDictionary   { get; set; }

        [Required]
        public ITaskItem[] Files { get; set; }
        //---------------------------------------------------------------------
        public Translator()
        {
            this.DictionaryHasHeader = true;
            this.ColumnInDictionary  = 1;
        }
        //---------------------------------------------------------------------
        public override bool Execute()
        {
            if (!File.Exists(this.Dictionary))
            {
                this.Log.LogError("Dictionary for '{0}' does not exist. It is expected to be at {1}", Path.GetFileName(this.Dictionary), this.Dictionary);
                return false;
            }

            if (this.Files.Length == 0) return !this.Log.HasLoggedErrors;
#if DEBUG
            this.Log.LogMessage(MessageImportance.High, "--------------------- #### ---------------------");
            this.Log.LogMessage(MessageImportance.High, this.Dictionary);
            this.Log.LogMessage(MessageImportance.High, string.Join(";", this.Files.Select(it => it.ItemSpec)));
            this.Log.LogMessage(MessageImportance.High, "--------------------- #### ---------------------");

            string msg = string.Format(
                "Dictionary: {0}\n" +
                "Files:      {1}\n" +
                "Time:       {2}\n" +
                "-----------------------------------\n",
                this.Dictionary,
                string.Join(";", this.Files.Select(it => it.ItemSpec)),
                DateTime.Now);

            File.AppendAllText(
                Path.Combine(Path.GetDirectoryName(typeof(Translator).Assembly.Location), "logTranslator.txt"),
                msg);
#endif
            try
            {
                var dictionary = new Dictionary(this.Dictionary, this.DictionaryHasHeader);
                var transDic   = dictionary.ReadTokensFromCsv(this.ColumnInDictionary);
                var translator = new gfoidl.Translator.Translator(transDic);

                translator.Translate(this.Files.Select(it => it.ItemSpec));
            }
            catch (FileNotFoundException fex)
            {
                this.Log.LogError("{0} -> {1}", fex.Message, fex.FileName);
            }
            catch (Exception ex)
            {
                this.Log.LogErrorFromException(ex);
            }

            return !this.Log.HasLoggedErrors;
        }
    }
}