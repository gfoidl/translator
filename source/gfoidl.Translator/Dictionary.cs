﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.IO;
using System.Text;

namespace gfoidl.Translator
{
    public class Dictionary
    {
        private readonly string _fileName;
        private readonly bool   _hasHeader;
        //---------------------------------------------------------------------
        public string FileName  { get { return _fileName; } }
        public bool HasHeader   { get { return _hasHeader; } }
        internal string Culture { get { return Path.GetFileNameWithoutExtension(_fileName); } }
        //---------------------------------------------------------------------
        public Dictionary(string fileName, bool hasHeader = true)
        {
            if (fileName == null)
                throw new ArgumentNullException("fileName");
            Contract.EndContractBlock();

            if (!Path.HasExtension(fileName))
                fileName = Path.Combine(fileName, "dictionary.csv");

            _fileName  = fileName;
            _hasHeader = hasHeader;
        }
        //---------------------------------------------------------------------
        public void WriteTokensToCsv(IEnumerable<string> tokens, bool appendDummyTranslation = true, char separator = ';')
        {
            if (tokens == null)
                throw new ArgumentNullException("tokens");
            Contract.EndContractBlock();

            Directory.CreateDirectory(Path.GetDirectoryName(_fileName));
            using (StreamWriter sw = File.CreateText(_fileName))
            {
                if (_hasHeader)
                    sw.WriteLine("token{0}translation", separator);

                foreach (string token in tokens)
                {
                    string tmp = token.EnsureQuotesByMultiline();

                    if (appendDummyTranslation)
                        sw.WriteLine("{0}{1}{0}", tmp, separator);
                    else
                        sw.WriteLine("{0}{1}", tmp, separator);
                }
            }
        }
        //---------------------------------------------------------------------
        public Dictionary<string, string> ReadTokensFromCsv(int column = 1, char separator = ';')
        {
            if (column < 1)
                throw new ArgumentOutOfRangeException("column", "Column must be greater or equal to 1. Column 0 is the token, so starting with 1 the translations are listed.");
            Contract.EndContractBlock();

            Dictionary<string, string> dic = new Dictionary<string, string>();

            using (StreamReader sr = File.OpenText(_fileName))
            {
                int rowNumber = 0;
                foreach (List<string> cols in sr.ReadColsFromMultiline(separator))
                {
                    if (rowNumber++ == 0 && _hasHeader) continue;

                    if (column >= cols.Count)
                        throw new ArgumentException(
                            string.Format("Column (zero-based) {0} does not exist in file '{1}' line {2}. Check if separator '{3}' is correctly, and for multiline strings check if quotes are set.", column, _fileName, rowNumber, separator),
                            "column");

                    dic[cols[0]] = cols[column];
                }
            }

            return dic;
        }
    }
}