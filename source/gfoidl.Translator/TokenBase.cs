﻿using System;
using System.Diagnostics.Contracts;

namespace gfoidl.Translator
{
    public abstract class TokenBase
    {
        protected string _pattern = @"\${((?:[^{}]|(?<counter>{)|(?<-counter>}))*)(?(counter)(?!))}";
        //---------------------------------------------------------------------
        public string Pattern
        {
            get { return _pattern; }
            set
            {
                if (string.IsNullOrWhiteSpace(value))
                    throw new ArgumentNullException("value");
                Contract.EndContractBlock();

                _pattern = value;
            }
        }
    }
}