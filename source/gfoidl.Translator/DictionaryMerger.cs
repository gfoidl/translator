﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;

namespace gfoidl.Translator
{
    public class DictionaryMerger
    {
        private readonly string _mergedDictionaryFile;
        //---------------------------------------------------------------------
        public string MergedDicionaryFile { get { return _mergedDictionaryFile; } }
        //---------------------------------------------------------------------
        public DictionaryMerger(string mergedDictionaryFile)
        {
            if (mergedDictionaryFile == null)
                throw new ArgumentNullException("mergedDictionaryFile");
            Contract.EndContractBlock();

            _mergedDictionaryFile = mergedDictionaryFile;
        }
        //---------------------------------------------------------------------
        public void MergeDictionaries(IEnumerable<Dictionary> dictionaries)
        {
            Tuple<string, Dictionary<string, string>>[] dics = GetOrderedDics(dictionaries);
            string[] tokens                                  = RetrieveTokens(dics.Select(t => t.Item2));

            using (StreamWriter sw = File.CreateText(_mergedDictionaryFile))
            {
                sw.WriteLine("token;{0}", string.Join(";", dics.Select(t => t.Item1)));

                // gibt über ein T[] auch eine for-Schleife vom Compiler :-)
                // und `token` zu schreiben ist netter als `tokens[i]` ;-)
                foreach (string token in tokens)
                {
                    sw.Write("{0};", token);

                    for (int i = 0; i < dics.Length; ++i)
                    {
                        string word = string.Empty;
                        dics[i].Item2.TryGetValue(token, out word);

                        sw.Write("{0}{1}", word.EnsureQuotesByMultiline(), i < dics.Length - 1 ? ";" : string.Empty);
                    }

                    sw.WriteLine();
                }
            }
        }
        //---------------------------------------------------------------------
        public IReadOnlyCollection<string> SplitDictionary(string outputPath)
        {
            if (outputPath == null)
                throw new ArgumentNullException("outputPath");
            Contract.EndContractBlock();

            Directory.CreateDirectory(outputPath);
            using (StreamReader sr = File.OpenText(_mergedDictionaryFile))
            {
                return SplitDictionaryCore(sr, outputPath).AsReadOnly();
            }
        }
        //---------------------------------------------------------------------
        #region Private Members
        internal static Tuple<string, Dictionary<string, string>>[] GetOrderedDics(IEnumerable<Dictionary> dictionaries)
        {
            var dics = dictionaries
                .OrderBy(d => d.Culture)
                .Select(d => Tuple.Create(d.Culture, d.ReadTokensFromCsv()))
                .ToArray();

            return dics;
        }
        //---------------------------------------------------------------------
        internal static string[] RetrieveTokens(IEnumerable<Dictionary<string, string>> dics)
        {
            string[] tokens = dics
                .SelectMany(d => d.Keys)
                .Distinct()
                .OrderBy(t => t)
                .ToArray();

            return tokens;
        }
        //---------------------------------------------------------------------
        private static List<string> SplitDictionaryCore(StreamReader sr, string outputPath)
        {
            var rowEnumerator = sr.ReadColsFromMultiline().GetEnumerator();
            rowEnumerator.MoveNext();
            List<string> row  = rowEnumerator.Current;

            var writerTuples       = CreateWriters(outputPath, row).ToArray();
            StreamWriter[] writers = writerTuples.Select(t => t.Item2).ToArray();

            try
            {
                while (rowEnumerator.MoveNext())
                {
                    row = rowEnumerator.Current;

                    for (int i = 0; i < writers.Length; ++i)
                        writers[i].WriteLine("{0};{1}", row[0].EnsureQuotesByMultiline(), row[i + 1].EnsureQuotesByMultiline());
                }
            }
            finally
            {
                foreach (StreamWriter sw in writers)
                    sw.Dispose();
            }

            return writerTuples.Select(t => t.Item1).ToList();
        }
        //---------------------------------------------------------------------
        private static IEnumerable<Tuple<string, StreamWriter>> CreateWriters(string outputPath, List<string> header)
        {
            // 0 -> token, 1...n -> culture
            for (int i = 1; i < header.Count; ++i)
            {
                string culture    = header[i];
                string outputFile = Path.Combine(outputPath, culture);
                outputFile        = Path.ChangeExtension(outputFile, "csv");
                StreamWriter sw   = File.CreateText(outputFile);

                // Header schreiben:
                sw.WriteLine("token;translation");

                yield return Tuple.Create(culture, sw);
            }
        }
        #endregion
    }
}