﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics.Contracts;
using System.Globalization;
using System.Reflection;
using System.Reflection.Emit;

namespace gfoidl.Translator
{
    public static class ObjectLocalizer
    {
        private delegate string FastGetter(object target);
        private static ConcurrentDictionary<string, FastGetter> _map;
        //---------------------------------------------------------------------
        static ObjectLocalizer()
        {
            _map = new ConcurrentDictionary<string, FastGetter>();
        }
        //---------------------------------------------------------------------
        public static string Get<T>(T model, string propertyName, CultureInfo culture = null)
        {
            return Get(typeof(T), model, propertyName, culture);
        }
        //---------------------------------------------------------------------
        public static string Get(Type type, object model, string propertyName, CultureInfo culture = null)
        {
            if (type == null) throw new ArgumentNullException("type");
            if (model == null) throw new ArgumentNullException("model");
            if (string.IsNullOrWhiteSpace(propertyName)) throw new ArgumentNullException("propertyName");
            Contract.EndContractBlock();

            if (culture == null) culture = CultureInfo.CurrentCulture;

            FastGetter getter = GetFastGetter(type, model, propertyName, culture);

            return getter(model);
        }
        //---------------------------------------------------------------------
        private static FastGetter GetFastGetter(Type type, object model, string propertyName, CultureInfo culture)
        {
            string key = string.Format("{0}@{1}@{2}", culture.Name, propertyName, type.AssemblyQualifiedName);

            return _map.GetOrAdd(key, k =>
            {
                var pi     = GetMatchingPropertyInfo(type, model, propertyName, culture);
                var getter = CreateFastGetterDelegate(pi);

                return getter;
            });
        }
        //---------------------------------------------------------------------
        private static PropertyInfo GetMatchingPropertyInfo(Type type, object model, string propertyName, CultureInfo culture)
        {
            PropertyInfo pi = null;
            while (culture != null && culture.Name != string.Empty)
            {
                pi = type.GetProperty(propertyName + "_" + culture.Name.Replace("-", string.Empty));

                if (pi != null) break;

                culture = culture.Parent;
            }

            if (pi == null) pi = type.GetProperty(propertyName);

            return pi;
        }
        //---------------------------------------------------------------------
        private static FastGetter CreateFastGetterDelegate(PropertyInfo propertyInfo)
        {
            MethodInfo getMethod = propertyInfo.GetGetMethod();
            Type[] arguments     = { typeof(object) };
            DynamicMethod dm     = new DynamicMethod("_Get" + propertyInfo.Name, typeof(string), arguments, propertyInfo.DeclaringType);
            ILGenerator ilGen    = dm.GetILGenerator();

            ilGen.Emit(OpCodes.Ldarg_0);
            ilGen.Emit(OpCodes.Callvirt, getMethod);
            ilGen.Emit(OpCodes.Ret);

            return dm.CreateDelegate(typeof(FastGetter)) as FastGetter;
        }
    }
}