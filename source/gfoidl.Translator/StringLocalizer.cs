﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Runtime.CompilerServices;

namespace gfoidl.Translator
{
    public static class StringLocalizer
    {
        private static Dictionary<string, Dictionary<string, string>> _dicMap;
        //---------------------------------------------------------------------
        public static string BasePathForDictionaries { get; set; }
        //---------------------------------------------------------------------
        [MethodImpl(MethodImplOptions.Synchronized)]
        private static void Init()
        {
            // Double check -- könnte ja mittlerweile schon initiiert sein
            if (_dicMap != null) return;

            string dictionaries = ConfigurationManager.AppSettings["dictionaries"];

            if (string.IsNullOrWhiteSpace(dictionaries))
                throw new ArgumentNullException("When StringLocalizer is used, a value for dictionaries has to be set in configuration/appSettings. Delimit several dictionaries with ;");

            _dicMap = new Dictionary<string, Dictionary<string, string>>();

            foreach (string dictionary in dictionaries.Split(';'))
            {
                string culture = Path.GetFileNameWithoutExtension(dictionary);

                string file = dictionary;
                if (BasePathForDictionaries != null)
                    file = Path.Combine(BasePathForDictionaries, file);

                var dic = new Dictionary(file).ReadTokensFromCsv();

                _dicMap.Add(culture, dic);
            }
        }
        //---------------------------------------------------------------------
        public static string Get(string token, CultureInfo culture = null)
        {
            if (_dicMap == null) Init();
            if (culture == null) culture = CultureInfo.CurrentCulture;

            string tmp  = null;

            while (culture != null && culture.Name != string.Empty)
            {
                tmp = GetCore(culture.Name, token);
                if (tmp != null) return tmp;

                culture = culture.Parent;
            }

            string defaultCulture = ConfigurationManager.AppSettings["defaultCulture"];
            tmp                   = GetCore(defaultCulture, token);
            if (tmp != null) return tmp;

            throw new ArgumentException(string.Format(
                "For token '{0}' no entry found in dictionary '{1}'.",
                token, CultureInfo.CurrentCulture.Name + ".csv"));
        }
        //---------------------------------------------------------------------
        private static string GetCore(string cultureName, string token)
        {
            if (string.IsNullOrWhiteSpace(cultureName)) return null;

            Dictionary<string, string> dic;
            if (_dicMap.TryGetValue(cultureName, out dic))
            {
                string s;
                if (dic.TryGetValue(token, out s))
                    return s;
            }

            return null;
        }
    }
}