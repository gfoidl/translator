﻿using System.Collections.Generic;
using System.IO;
using System.Text;

namespace gfoidl.Translator
{
    internal static class MyExtensions
    {
        public static string EnsureQuotesByMultiline(this string input)
        {
            if (string.IsNullOrWhiteSpace(input) || !input.Contains("\n"))
                return input;

            return "\"" + input + "\"";
        }
        //---------------------------------------------------------------------
        public static IEnumerable<List<string>> ReadColsFromMultiline(this TextReader reader, char seperator = ';')
        {
            bool isInQuote   = false;
            List<string> row = new List<string>();
            StringBuilder sb = new StringBuilder();

            while (reader.Peek() != -1)
            {
                char c = (char)reader.Read();

                if (c == '"')
                {
                    // Anfang der Spalte
                    if (sb.Length == 0)
                    {
                        isInQuote = true;
                        continue;
                    }

                    // Ende der Spalte:
                    int peek = reader.Peek();       // würde gleich auf char gecastet, so kann 65535 (~ -1) rauskommen
                    if (peek == -1 || (char)peek == seperator || (char)peek == '\r' || (char)peek == '\n')
                        isInQuote = false;
                    else
                        sb.Append(c);

                    continue;
                }

                // Spalte erreicht
                if (!isInQuote && c == seperator)
                {
                    row.Add(sb.ToString());
                    sb.Clear();
                    continue;
                }

                // Zeilenende erreicht Dos: \r\n Unix: \n Mac: \r
                if (!isInQuote && (c == '\r' || c == '\n'))
                {
                    // Wenns \r ist, schauen ob \n folgt
                    if (c == '\r')
                    {
                        int peek = reader.Peek();

                        // Wenns \n ist, so einfach weglesen.
                        if (peek != -1 && ((char)peek == '\n'))
                            c = (char)reader.Read();
                    }

                    row.Add(sb.ToString());

                    if (ShouldRowBeReturned(row))
                        yield return row;

                    row = new List<string>();
                    sb.Clear();
                    continue;
                }

                if (c != '\r')
                    sb.Append(c);
            }

            if (sb.Length > 0)
            {
                row.Add(sb.ToString());

                if (ShouldRowBeReturned(row))
                    yield return row;
            }
        }
        //---------------------------------------------------------------------
        private static bool ShouldRowBeReturned(List<string> row)
        {
            return !row[0].StartsWith("#") && row[0].Length > 0;
        }
    }
}