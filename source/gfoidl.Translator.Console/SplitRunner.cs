﻿namespace gfoidl.Translator
{
    internal class SplitRunner : Runner
    {
        public SplitRunner(string[] args) : base(args, 2) { }
        //---------------------------------------------------------------------
        protected override void RunCore()
        {
            this.LogMessage("in split mode");

            DictionaryMerger splitter = new DictionaryMerger(_args[0]);

            this.LogMessage("splitting dictionary...");
            var cultures = splitter.SplitDictionary(_args[1]);
            this.LogMessage("dictionary splitted to cultures {0}.", string.Join(", ", cultures));
        }
    }
}