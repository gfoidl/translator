﻿using System;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace gfoidl.Translator
{
    [DebuggerNonUserCode]
    public static class ConsoleFormatter
    {
        public static void ConsoleWriteFormatted(this string line)
        {
            const string pattern = @"#(\w{1,2}){((?:[^{}]|(?<counter>{)|(?<-counter>}))*(?(counter)(?!)))}";

            MatchCollection matches = Regex.Matches(line, pattern);

            if (matches.Count == 0)
            {
                Console.WriteLine(line);
                return;
            }

            Console.Write(line.Substring(0, matches[0].Index));

            for (int i = 0; i < matches.Count; ++i)
            {
                var oldColor            = Console.ForegroundColor;
                var newColor            = GetNewColor(matches[i].Groups[1].Value) ?? oldColor;
                Console.ForegroundColor = newColor;

                Console.Write(matches[i].Groups[2].Value);

                Console.ForegroundColor = oldColor;

                if ((i + 1) < matches.Count)
                {
                    int start = matches[i].Index + matches[i].Length;
                    int len   = matches[i + 1].Index - start;
                    Console.Write(line.Substring(start, len));
                }
            }

            Console.Write(line.Substring(matches[matches.Count - 1].Index + matches[matches.Count - 1].Length));

            Console.WriteLine();
        }
        //---------------------------------------------------------------------
        private static ConsoleColor? GetNewColor(string color)
        {
            switch (color)
            {
                case "y" : return ConsoleColor.Yellow;
                case "r" : return ConsoleColor.Red;
                case "b" : return ConsoleColor.Blue;
                case "g" : return ConsoleColor.Green;
                case "dy": return ConsoleColor.DarkYellow;
                case "dr": return ConsoleColor.DarkRed;
                case "db": return ConsoleColor.DarkBlue;
                case "dg": return ConsoleColor.DarkGreen;
                default  : return null;
            }
        }
    }
}