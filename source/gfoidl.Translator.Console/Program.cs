﻿using System;
using System.Diagnostics;
using System.Linq;

namespace gfoidl.Translator
{
    static class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Runner runner = GetRunner(args);

                runner.Run();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            Console.WriteLine("\nbye");
            if (Debugger.IsAttached)
                Console.ReadKey();
        }
        //---------------------------------------------------------------------
        private static Runner GetRunner(string[] args)
        {
            if (args == null || args.Length < 2)
                return new HelpRunner();

            string mode = args[0];

            switch (mode)
            {
                case "translate": return new TranslationRunner(args.GetArgsInternal());
                case "extract": return new ExtractionRunner(args.GetArgsInternal());
                case "split": return new SplitRunner(args.GetArgsInternal());
                case "merge": return new MergeRunner(args.GetArgsInternal());
                default:
                    return new HelpRunner();
            }
        }
        //---------------------------------------------------------------------
        private static string[] GetArgsInternal(this string[] args)
        {
            return args.Skip(1).ToArray();
        }
    }
}