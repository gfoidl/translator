﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace gfoidl.Translator
{
    internal abstract class Runner
    {
        protected string[] _args;
        private bool _isVerbose;
        private int _numberOfArgsExpected;
        //---------------------------------------------------------------------
        protected Runner() { }
        protected Runner(string[] args, int numberOfArgsExpected)
        {
            _args                 = args;
            _isVerbose            = args.Any(s => s == "-v");
            _numberOfArgsExpected = numberOfArgsExpected;

            if (_isVerbose)
                _args = _args.Where(s => s != "-v").ToArray();
        }
        //---------------------------------------------------------------------
        public virtual void Run()
        {
            if (_args.Length != _numberOfArgsExpected)
            {
                "#r{Unknown number of arguments given -- see help for more information}".ConsoleWriteFormatted();
                Environment.Exit(1);
            }

            this.RunCore();
        }
        //---------------------------------------------------------------------
        protected abstract void RunCore();
        //---------------------------------------------------------------------
        [DebuggerStepThrough]
        protected void LogMessage(string message, params object[] args)
        {
            if (_isVerbose)
                string.Format(message, args).ConsoleWriteFormatted();
        }
        //---------------------------------------------------------------------
        protected void LogErrorAndExit(int exitCode, string message, params object[] args)
        {
            message = string.Format(message, args);
            ("#r{" + message + "}").ConsoleWriteFormatted();
            Environment.Exit(exitCode);
        }
        //---------------------------------------------------------------------
        protected IEnumerable<string> GatherFiles()
        {
            string[] data = _args[0].Split(';');

            foreach (string trial in data)
            {
                if (File.Exists(trial))
                {
                    yield return trial;
                    continue;
                }

                if (Directory.Exists(trial))
                    foreach (string f in Directory.GetFiles(trial, "*.*", SearchOption.AllDirectories))
                        yield return f;
            }
        }
    }
}