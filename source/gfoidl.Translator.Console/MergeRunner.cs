﻿using System.Linq;

namespace gfoidl.Translator
{
    internal class MergeRunner : Runner
    {
        public MergeRunner(string[] args) : base(args, 2) { }
        //---------------------------------------------------------------------
        protected override void RunCore()
        {
            this.LogMessage("in merge mode");

            this.LogMessage("reading dictionaries...");
            var dictionaries = _args[1].Split(';').Select(s => new Dictionary(s)).ToList();
            this.LogMessage("{0} dictionaries read.", dictionaries.Count);

            DictionaryMerger merger = new DictionaryMerger(_args[0]);

            this.LogMessage("mergind dictionaries...");
            merger.MergeDictionaries(dictionaries);
            this.LogMessage("dictionaries merged to {0}", _args[0]);
        }
    }
}