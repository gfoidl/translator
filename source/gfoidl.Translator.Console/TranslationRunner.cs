﻿using System.Collections.Generic;
using System.Linq;

namespace gfoidl.Translator
{
    internal class TranslationRunner : Runner
    {
        public TranslationRunner(string[] args) : base(args, 3) { }
        //---------------------------------------------------------------------
        protected override void RunCore()
        {
            int column;
            if (!int.TryParse(_args[2], out column))
                this.LogErrorAndExit(2, "argument given for column-index-of-translation-in-dictionary is not a number");

            this.LogMessage("in translation mode.");

            this.LogMessage("gathering files...");
            List<string> filesToTranslate = this.GatherFiles().ToList();
            this.LogMessage("\t{0} files gathered.", filesToTranslate.Count);

            this.LogMessage("reading dictionary...");
            Dictionary dic                        = new Dictionary(_args[1]);
            Dictionary<string, string> dictionary = dic.ReadTokensFromCsv(column);
            this.LogMessage("\tdictionary read.");

            this.LogMessage("translating...");
            Translator translator = new Translator(dictionary);
            translator.Translate(filesToTranslate);

            string message = string.Format("{0} files translated.", filesToTranslate.Count);
            ("#dg{" + message + "}").ConsoleWriteFormatted();
        }
    }
}