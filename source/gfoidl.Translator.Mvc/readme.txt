﻿Thanks for using gfoidl.Translator.Mvc!

1. In web.config the usage of the clients culture was enabled. You can turn off when you don't need this.

<system.web>
    <globalization enableClientBasedCulture="true" culture="auto" uiCulture="auto" />
    ...

2. _ViewStart.cshtml may be updated to

@{
    Layout = "~/Localized/Views/${_culture}/Shared/_Layout.cshtml";
}

So that the Layout according to the culture is used.