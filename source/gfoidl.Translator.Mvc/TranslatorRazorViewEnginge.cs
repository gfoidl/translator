﻿using System;
using System.Configuration;
using System.Web.Mvc;

namespace gfoidl.Translator.Mvc
{
    public class TranslatorRazorViewEnginge : RazorViewEngine
    {
        private readonly string _viewBaseDirNotRelative;
        private readonly string _viewBaseDir;
        //---------------------------------------------------------------------
        public TranslatorRazorViewEnginge()
        {
            string translatedBaseDir = ConfigurationManager.AppSettings["translatedBaseDir"];

            if (string.IsNullOrWhiteSpace(translatedBaseDir))
                throw new ArgumentNullException("In appSettings a value for translatedBaseDir has to be provided.");

            _viewBaseDirNotRelative = translatedBaseDir.EnforceNoAppRelativePath() + "/Views";
            _viewBaseDir            = translatedBaseDir.EnforceAppRelativePath() + "/Views";
        
            // %1 ist normal Views bzw. leer -> hier wird es aber je nach Culture ersetzt nach z.B. Localized/Views/de
            this.AreaViewLocationFormats = new[] {
                "~/Areas/{2}/Views/%1/{1}/{0}.cshtml",
                "~/Areas/{2}/Views/%1/Shared/{0}.cshtml",
            };

            this.AreaMasterLocationFormats = new[] {
                "~/Areas/{2}/%1/{1}/{0}.cshtml",
                "~/Areas/{2}/%1/Shared/{0}.cshtml",
            };

            this.AreaPartialViewLocationFormats = new[] {
                "~/Areas/{2}/%1/{1}/{0}.cshtml",
                "~/Areas/{2}/%1/Shared/{0}.cshtml",
            };

            this.ViewLocationFormats = new[] {
                "~/%1/{1}/{0}.cshtml",
                "~/%1/Shared/{0}.cshtml",
            };

            this.MasterLocationFormats = new[] {
                "~/%1/{1}/{0}.cshtml",
                "~/%1/Shared/{0}.cshtml",
            };

            this.PartialViewLocationFormats = new[] {
                "~/%1/{1}/{0}.cshtml",
                "~/%1/Shared/{0}.cshtml",
            };
        }
        //---------------------------------------------------------------------
        protected override IView CreateView(ControllerContext controllerContext, string viewPath, string masterPath)
        {
            string path = this.GetViewPath(controllerContext);
            return base.CreateView(controllerContext, viewPath.Replace("%1", path), masterPath.Replace("%1", path));
        }
        //---------------------------------------------------------------------
        protected override IView CreatePartialView(ControllerContext controllerContext, string partialPath)
        {
            string path = this.GetViewPath(controllerContext);
            return base.CreatePartialView(controllerContext, partialPath.Replace("%1", path));
        }
        //---------------------------------------------------------------------
        protected override bool FileExists(ControllerContext controllerContext, string virtualPath)
        {
            string path = this.GetViewPath(controllerContext);
            bool exists = base.FileExists(controllerContext, virtualPath.Replace("%1", path));
            return exists;
        }
        //---------------------------------------------------------------------
        private string GetViewPath(ControllerContext controllerContext)
        {
            return _viewBaseDirNotRelative + "/" + PathHelper.GetAvailableCulture(_viewBaseDir, controllerContext.HttpContext.Server.MapPath);
        }
    }
}