param($installPath, $toolsPath, $package, $project)

$name = $project.Name + ".wpp.targets"

If (Test-Path $name) {
    $file = $project.ProjectItems.Item($name)
    #$file.Name = "default.nuspec-Template"         w�re auch m�glich, da es dann von NuGet entfernt wird -> ich entferne es aber gleich direkt
    $file.Delete()
}