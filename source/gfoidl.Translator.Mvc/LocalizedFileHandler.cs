﻿using System;
using System.Configuration;
using System.IO;
using System.Text.RegularExpressions;
using System.Web;
using MimeTypes;

namespace gfoidl.Translator.Mvc
{
    public class LocalizedFileHandler : IHttpHandler
    {
        private readonly string _baseDir;
        //---------------------------------------------------------------------
        public LocalizedFileHandler()
        {
            string translatedBaseDir = ConfigurationManager.AppSettings["translatedBaseDir"];

            if (string.IsNullOrWhiteSpace(translatedBaseDir))
                throw new ArgumentNullException("In appSettings a value for translatedBaseDir has to be provided.");

            _baseDir = translatedBaseDir.EnforceAppRelativePath();
        }
        //---------------------------------------------------------------------
        #region IHttpHandler Members
        public bool IsReusable
        {
            get { return true; }
        }
        //---------------------------------------------------------------------
        public void ProcessRequest(HttpContext context)
        {
            string relativePath = context.Request.AppRelativeCurrentExecutionFilePath.Replace(".localized", string.Empty);
            Match match         = Regex.Match(relativePath, @"^~/([^/]+)/(.+)$", RegexOptions.Compiled | RegexOptions.IgnoreCase);
            string culture      = PathHelper.GetAvailableCulture(_baseDir + "/" + match.Groups[1].Value, context.Server.MapPath);

            string newPath = string.Format(
                "{0}/{1}/{2}/{3}",
                _baseDir,
                match.Groups[1].Value,
                culture,
                match.Groups[2].Value);

            string mappedPath = context.Server.MapPath(newPath);

            if (!File.Exists(mappedPath))
            {
                context.Response.StatusCode = 404;

                if (context.IsDebuggingEnabled)
                    context.Response.StatusDescription = string.Format("Not found -> {0}", mappedPath);

                context.Response.End();
                return;
            }

            context.Response.TransmitFile(mappedPath);

            SetContentType(context, mappedPath);
            SetCacheHeaders(context, mappedPath);
        }
        #endregion
        //---------------------------------------------------------------------
        internal static void SetContentType(HttpContext context, string file)
        {
            string contentType           = MimeTypeMap.GetMimeType(Path.GetExtension(file));
            context.Response.ContentType = contentType;
        }
        //---------------------------------------------------------------------
        private static void SetCacheHeaders(HttpContext context, string file)
        {
            context.Response.AddFileDependency(file);
            context.Response.Cache.SetCacheability(HttpCacheability.Public);
            context.Response.Cache.SetETagFromFileDependencies();
            context.Response.Cache.SetLastModifiedFromFileDependencies();
            context.Response.Cache.SetExpires(DateTime.Now.AddDays(7));
            context.Response.Cache.VaryByHeaders["Accept-Language"] = true;
            context.Response.Cache.VaryByHeaders["Accept-Encoding"] = true;
        }
    }
}
