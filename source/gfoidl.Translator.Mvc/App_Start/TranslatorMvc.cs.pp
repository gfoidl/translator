﻿using System.Web.Mvc;
using System.Web.Routing;
using gfoidl.Translator.Mvc;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof($rootnamespace$.App_Start.TranslatorMvc), "PreStart")]

namespace $rootnamespace$.App_Start 
{
	public static class TranslatorMvc
	{
		public static void PreStart()
		{
			ViewEngines.Engines.Clear();
			ViewEngines.Engines.Add(new TranslatorRazorViewEnginge());

			RouteTable.Routes.IgnoreRoute("{*allLocalized}", new { allLocalized = @".*\.localized(/.*)?" });
		}
	}
}