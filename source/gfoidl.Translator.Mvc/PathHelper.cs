﻿using System;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Text.RegularExpressions;

namespace gfoidl.Translator.Mvc
{
    internal static class PathHelper
    {
        private static string _stickToCulture = ConfigurationManager.AppSettings["stickToCulture"];
        //---------------------------------------------------------------------
        public static string EnforceAppRelativePath(this string path)
        {
            if (path.StartsWith("~/")) return path;
            if (path.StartsWith("/")) return "~" + path;
            return "~/" + path;
        }
        //---------------------------------------------------------------------
        public static string EnforceNoAppRelativePath(this string path)
        {
            return Regex.Replace(path, "[~/]", string.Empty);
        }
        //---------------------------------------------------------------------
        public static string GetAvailableCulture(string baseDir, Func<string, string> mapPath)
        {
            return GetAvailableCulture(baseDir, mapPath, _stickToCulture);
        }
        //---------------------------------------------------------------------
        internal static string GetAvailableCulture(string baseDir, Func<string, string> mapPath, string stickToCulture)
        {
            if (!string.IsNullOrWhiteSpace(stickToCulture))
                return stickToCulture;

            var culture = CultureInfo.CurrentCulture;
            while (culture != null && culture.Name != string.Empty)
            {
                string test = baseDir + "/" + culture.Name;
                test        = mapPath(test);

                if (Directory.Exists(test))
                    return culture.Name;

                culture = culture.Parent;
            }

            return ConfigurationManager.AppSettings["defaultCulture"];
        }
    }
}