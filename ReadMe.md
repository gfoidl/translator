[![Build status](https://ci.appveyor.com/api/projects/status/onud4srifalxaxkr/branch/master?svg=true)](https://ci.appveyor.com/project/GntherFoidl/translator/branch/master)

Template-based translation with dictionaries.

Strings surrounded with `${` `}` in the templates are translated by a lookup in the (provided) dictionary.
```
templates
+-foo.txt
+-bar.txt
```
turns with cultures _de_, and _en_ into
```
data
+-de
 +-foo.txt
 +-bar.txt
+-en
 +-foo.txt
 +-bar.txt
```

# Contents

[TOC]

---
# Translator (.net Console-application)

## General

A command-line tool for template-based translation with dictionaries.  

Translator has two (basic) modes:  

* _translate_  
* _extract_

## Basic Workflow

1. create template(s) -- this can be any text-based file  

2. surround the strings, that should be replaced -- called tokens, with `${ }`.  
   For instance `${my} ${Name}` or `${xyz}` or whole sentences.    

3. build a dictionary by running `Translator extract path-to-template(s) path-to-dictionary`  

4. a generic dicionary.csv is generated, that contains the tokens  
   (alphabetically ordered) in the format `token;translation`  

5. localize the content of this dictionary, so that in the translation-column
   the translated word for the takens are placed  
   There can be as many translation-columns as you want  
   (typically there's only one)  
   For instance:  
   `token;en;de;it`  
   or only:  
   `token;en`  

6. copy the templates to the desired path for the next step

7. translate the template(s) by running  
   `Translator translate path-to-copied-templates path-to-dictionary column-index-of-translation-in-dictionary`  

## Arguments

`path-to-template(s)` can be either a single file, a column separated (;)
set of files, or a folder containing the files.  
In case of a folder it is scanned recursiveley for (text-) files.  
For `path-to-copied-templates` the same rules apply.

If `path-to-dictionary` points to a folder the filename #dg{dictionary.csv} will be used.

`column-index` is zero-based, i.e. the token=0;en=1;de=2;it=3 from the above example

Switch `-v` is for verbosity.

## Additional Modes

### merge

Merges localized dictionaries to a single dictionary, containing all tokens (and their translations) found in the localized dictionaries.  
With the merged dictionary translation (e.g. in Excel) may be easier.

Usage: `Translator merge path-to-merged-dictionary path-to-dictionaries`  
`path-to-dictionaries` is a comma (`;`) separated list of files

### split

Splitting is the oppositve of merging -- a single merged dictionary is splitted in culture-specific single dictionaries

Usage: `Translator split path-to-merged-dictionary outputpath`  
The culture-specific dictionaries are writte to `outputpath`.

---
# MsBuild/xBuild-Target

## General

There is also a _MsBuild-Target_, that has basically the same function as the _Translator_ described above.  
The task can be installed via NuGet [gfoidl.Translator.Tasks](https://www.nuget.org/packages/gfoidl.Translator.Tasks) and the Build-Target gets injected in the project-file (by NuGet).

Before the actual build the template-files are copied to the destination (see below), and translated by selecting the appriate dictionary following the convention of matching culture-names.

The translation is done for every culture specified in the settings.

## Default Settings

By default, templates are expected to reside in `template`-folder, and all files in this folder are recursively included in the process of either extracting the tokens or translating based on the dictionary.  
For translation the files are copied to `data/{culture}`, where {culture} is replaced by the culture-identifier as specified by the user (see later). Default cultures are _de_, and _en_. Dictionary files are expected to reside in `strings`-folder with the name following the convention `{culture}.csv` -- so for instance `strings/de.csv`

## Options

Only basis knowledge of MsBuild/xBuild is necessary to set custom options.  
To set custom options you can edit the _gfoidl.Translator.Tasks.targets_ file directly, but this is discouraged because every NuGet-update/restore will override this file. 

### Modify csproj-file

The _csproj_-file is the project file for C#-projects, and it is a MsBuild/xBuild-file that serves as the entry point in building a project (for instance when you build in Visual Studio).

Set the properties at the end, after _gfoidl.Translator.Tasks.targets_ is imported, so that the custom-values override the default values (this is a behavior of MsBuild).

### {project}.wpp.targets or any other targets file

For background-information refer to [How to: Edit Deployment Settings in Publish Profile (.pubxml) Files and the .wpp.targets File in Visual Studio Web Projects](https://msdn.microsoft.com/en-us/library/ff398069.aspx#wpp_targetst_file)

Do the customization within a target like the following:
```
#!xml
<Target Name="myInit" BeforeTargets="InitTranslation">
    <!-- -->
</Target>
```
For the translator-task it is important to speifiy `BeforeTargets="InitTranslation"` so that the correct target-order is established.

### Cultures

In this example the german (de) culture is removed from the project, and the italian (it) culture is added.  
Notice: the identifier of the culture is up to you. It is only important that the name of the dictionary corresponds to the culture.
```
#!xml
<ItemGroup Label="Cultures">
    <Culture Remove="de" />
    <Culture Include="en" />
    <Culture Include="it" />
</ItemGroup>
```

### Properties that can be set

Here the default properties are shown.
```
#!xml
<PropertyGroup>
    <TemplateDir>template</TemplateDir>
    <TemplateExtension>*</TemplateExtension>
    <DataDir>data</DataDir>
    <StringsDir>strings</StringsDir>
    <IsDebug>false</IsDebug>
    <TranslatorTmpDir>$(BaseIntermediateOutputPath)Translator_tmp</TranslatorTmpDir>
</PropertyGroup>
```

### Hooking-in the target

```
#!xml
<!-- Entry-point for translator -->
<Target Name="Translate" BeforeTargets="BeforeBuild">
    <!-- -->
</Target>

<!-- Entry for each culture -->
<Target Name="InitTranslation">
    <!-- -->
</Target>
```

For any kind of post-processing hook up to _Translate_.  
For settings properties, etc, hook up to _InitTranslation_.

---
# Asp.net MVC projects

Install the NuGet-package NuGet [gfoidl.Translator.Mvc](https://www.nuget.org/packages/gfoidl.Translator.Mvc), read the readme.txt and you're done. At least when you stick with the used default values.

## What happens to your project?

1. {project}.wpp.targets are added to your project  
  You can modify this target to set custom values -- as described above
1. a custom _RazorViewEngine_ is injected and used  
1. in the _web.config_ custom settings are injected  
1. required references are maintained (NuGet). Basically to [gfoidl.Translator.Tasks](https://www.nuget.org/packages/gfoidl.Translator.Tasks)
 
## Configuration

The following snippets shows the config that is injected.
```
#!xml
<appSettings>
    <!-- Specify a culture that should always be used -->
    <add key="stickToCulture" value="" />
    <add key="translatedBaseDir" value="Localized" />
</appSettings>

<system.web>
    <globalization enableClientBasedCulture="true" culture="auto" uiCulture="auto" />
</system.web>

<system.webServer>
    <handlers>
        <add name="localizedFileHandler"
                path         ="*.localized"
                type         ="gfoidl.Translator.Mvc.LocalizedFileHandler"
                verb         ="GET"
                preCondition="managedHandler" />
    </handlers>
</system.webServer>
```

In __StickToCulture__ you can specify a culture that is always used. This can be useful if you deploy a culture to a VM.  
_Notice_: a "culture" can be anything, not only _de_, _en_, and so on. You might declare customer specific data as culture. It is only mandatory that the used culture is set in {project}.wpp.targets, used as name for the dictionaries, and so on. 

If __StickToCulture__ is an empty string, the client based culture is used.

Have a look at the web-demo and you'll see how it works.
